//
//  Constants.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 23/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation
import UIKit

struct K {
    
    struct URL {
        static let PATH         = "http://falkor-cda.bastian.globo.com"
        
        static let NEWS_FEED    = "/feeds/b904b430-123a-4f93-8cf4-5365adf97892/posts/page/%@"
    }
}
