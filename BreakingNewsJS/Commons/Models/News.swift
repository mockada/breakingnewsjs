//
//  News.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 19/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import ObjectMapper
import Foundation

class News: Mappable {
    
    var chapeu  : String?
    var image   : String?
    var summary : String?
    var title   : String?
    var url     : String?
    var age     : Int?
    var type    : String?
    
    required convenience init?(map: Map) {
        self.init()
    }

    func mapping(map: Map) {
        chapeu  <- map["content.chapeu.label"]
        image   <- map["content.image.url"]
        summary <- map["content.summary"]
        title   <- map["content.title"]
        url     <- map["content.url"]
        age     <- map["age"]
        type    <- map["type"]
    }
    
    func getFormattedAge() -> String {
        guard let seconds = age else {
            return ""
        }
        
        let hoursMinutesSeconds = secondsToHoursMinutesSeconds(seconds: seconds)
        
        if hoursMinutesSeconds.0 != 0 {
            return hoursMinutesSeconds.0.description + (hoursMinutesSeconds.0 > 1 ? " horas" : " hora") + " atrás"
        }
        if hoursMinutesSeconds.1 != 0 {
            return hoursMinutesSeconds.1.description + (hoursMinutesSeconds.1 > 1 ? " minutos" : " minuto") + " atrás"
        }
        return hoursMinutesSeconds.2.description + (hoursMinutesSeconds.2 > 1 ? " segundos" : " segundo") + " atrás"
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
