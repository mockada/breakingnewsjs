//
//  RoundCornersImageView.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 26/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import UIKit

class RoundCornersImageView: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupRoundCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupRoundCorners()
    }
    
    func setupRoundCorners() {
        self.layer.cornerRadius   = 5
    }
}
