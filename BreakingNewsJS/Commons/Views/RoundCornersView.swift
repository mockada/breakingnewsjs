//
//  RoundCornersView.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 19/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import UIKit

class RoundCornersView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupRoundCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupRoundCorners()
    }
    
    func setupRoundCorners() {
        self.layer.cornerRadius   = 5
    }
}
