//
//  NewsCell.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 19/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var chapeuLabel              : UILabel!
    @IBOutlet weak var newsImage                : UIImageView!
    @IBOutlet weak var newsTitleLabel           : UILabel!
    @IBOutlet weak var briefDescriptionLabel    : UILabel!
    @IBOutlet weak var age                      : UILabel!
    @IBOutlet weak var backgroundLayerView      : RoundCornersView!
    
    func setupContent(news: News) {
        chapeuLabel.text            = news.chapeu
        newsTitleLabel.text         = news.title
        briefDescriptionLabel.text  = news.summary
        age.text                    = news.getFormattedAge()
        newsImage.image             = nil
        
        guard let imageUrl = news.image, (imageUrl.contains("jpg") || imageUrl.contains("png") || imageUrl.contains("jpeg")), let url = URL(string: imageUrl) else {
            
            backgroundLayerView.backgroundColor = UIColor.white
            changeLabelsColorTo(UIColor.gray)
            return
        }
        backgroundLayerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        changeLabelsColorTo(UIColor.white)
        
        downloadImage(from: url)
    }
    
    func changeLabelsColorTo(_ color: UIColor) {
        chapeuLabel.textColor            = color
        newsTitleLabel.textColor         = color
        briefDescriptionLabel.textColor  = color
        age.textColor                    = color
    }
    
    func downloadImage(from url: URL) {
        getData(from: url) { data, response, error in
            
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {
                self.newsImage.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
