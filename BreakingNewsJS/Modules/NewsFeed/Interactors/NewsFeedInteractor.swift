//
//  NewsFeedInteractor.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 23/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation

class NewsFeedInteractor: NSObject {
    
    var output : NewsFeedInteractorOutput?
    
    
    init(output: NewsFeedInteractorOutput) {
        super.init()
        
        self.output = output
    }
    
    func foundNewsFeed(_ news: [News]) {
        output?.foundNewsFeed(news)
    }
    
    func errorInNewsFeedContent() {
        output?.showTryAgainView()
    }
}


// MARK: NewsFeedInteractorInput

extension  NewsFeedInteractor: NewsFeedInteractorInput {
    
    func findNewsFeed(page: Int) {
        
        NewsFeedService.shared.getNewsFeedByPage(page: page, success: { (news) in
                self.foundNewsFeed(news)
            
            }, failure: {
                self.errorInNewsFeedContent()
            }
        )
    }
}
