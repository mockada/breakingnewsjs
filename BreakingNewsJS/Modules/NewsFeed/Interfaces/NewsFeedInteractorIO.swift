//
//  NewsFeedInteractorIO.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 23/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation

protocol NewsFeedInteractorInput {
    func findNewsFeed(page: Int)
}

protocol NewsFeedInteractorOutput {
    func foundNewsFeed(_ news: [News])
    func showTryAgainView()
}
