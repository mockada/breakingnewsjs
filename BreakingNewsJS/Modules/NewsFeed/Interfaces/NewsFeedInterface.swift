//
//  NewsFeedInterface.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 23/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation

protocol NewsFeedViewInterface {
    func showLoading(_ show: Bool)
    func showNewsFeedContent(news: [News])
    func showTryAgainView(_ show: Bool)
}

protocol NewsFeedModuleInterface {
    func viewDidLoad()
    func loadNextPage()
    func loadContent(initCount: Bool?)
}
