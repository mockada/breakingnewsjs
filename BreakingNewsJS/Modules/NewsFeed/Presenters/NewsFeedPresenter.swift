//
//  NewsFeedPresenter.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 23/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation

class NewsFeedPresenter: NSObject {
    
    var newsFeedInteractor  : NewsFeedInteractorInput?
    var userInterface       : NewsFeedViewInterface?
    
    var page                : Int = 0
    
    init(userInterface: NewsFeedViewInterface) {
        super.init()
        
        self.newsFeedInteractor = NewsFeedInteractor(output: self)
        self.userInterface = userInterface
    }
    
    func setPage(initCount: Bool?) {
        guard let initCount = initCount else {
            page = page > 0 ? page : 1
            return
        }
        page = initCount ? 1 : page + 1
    }
}


//MARK: NewsFeedModuleInterface

extension NewsFeedPresenter: NewsFeedModuleInterface {
    
    func viewDidLoad() {
        loadContent(initCount: true)
    }
    
    func loadNextPage() {
        loadContent(initCount: false)
    }
    
    func loadContent(initCount: Bool?) {
        userInterface?.showTryAgainView(false)
        userInterface?.showLoading(true)
        setPage(initCount: initCount)
        newsFeedInteractor?.findNewsFeed(page: page)
    }
}


// MARK: NewsFeedInteractorOutput

extension NewsFeedPresenter: NewsFeedInteractorOutput {
    
    func foundNewsFeed(_ newsFeed: [News]) {
        self.userInterface?.showTryAgainView(false)
        self.userInterface?.showNewsFeedContent(news: newsFeed)
        userInterface?.showLoading(false)
    }
    
    func showTryAgainView() {
        userInterface?.showLoading(false)
        self.userInterface?.showTryAgainView(true)
    }
}
