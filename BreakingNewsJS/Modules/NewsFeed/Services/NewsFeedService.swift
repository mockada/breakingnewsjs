//
//  NewsFeedService.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 23/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class NewsFeedService {
    
    static let shared  = NewsFeedService()
    
    func getNewsFeedByPage(page: Int, success: @escaping ([News]) -> Void, failure: @escaping () -> Void) {
        let url = K.URL.PATH + String(format: K.URL.NEWS_FEED, page.description)
        
        Alamofire.request(url).responseJSON { response in
            
            guard let json = response.result.value as? [String : Any],
                let items = json["items"] as? [[String : Any]] else {
                    print("ERROR: NewsFeedService - getNewsFeedByPage - json parse")
                failure()
                return
            }
            let news : Array<News> = Mapper<News>().mapArray(JSONArray: items)
            
            let filteredNews = news.filter({
                guard let url = $0.url, !url.isEmpty else {
                    return false
                }
                guard let type = $0.type, type.elementsEqual("basico") else {
                    return false
                }
                return true
            })
            success(filteredNews)
        }
    }
}
