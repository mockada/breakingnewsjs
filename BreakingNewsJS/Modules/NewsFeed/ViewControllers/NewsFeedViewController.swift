//
//  NewsFeedViewController.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 19/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import UIKit

class NewsFeedViewController: UIViewController {
    
    @IBOutlet weak var newsFeedTableView    : UITableView!
    @IBOutlet weak var backgroundLayerView  : UIView!
    @IBOutlet weak var loadingIndicator     : UIActivityIndicatorView!
    @IBOutlet weak var tryAgainView         : UIControl!
    
    var news                                = [News]()
    
    var eventHandler                        : NewsFeedModuleInterface?
    
    let segueToNewsWebview                  = "segueToNewsWebview"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventHandler = NewsFeedPresenter(userInterface: self)
        eventHandler?.viewDidLoad()
    }
    
    @IBAction func tryAgainDidTap(_ sender: Any) {
        eventHandler?.loadContent(initCount: nil)
    }
    
    
    // MARK: SEGUE
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == segueToNewsWebview) {
            let selectedNews = sender as! News
            
            let newsViewController = segue.destination as! NewsWebViewController
            newsViewController.urlString = selectedNews.url
        }
    }
}


// MARK: UITableViewDelegate

extension NewsFeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedNews = news[indexPath.row]
        performSegue(withIdentifier: segueToNewsWebview, sender: selectedNews)
    }
}


// MARK: UITableViewDataSource

extension NewsFeedViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = newsFeedTableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell else {
            return UITableViewCell()
        }
        cell.setupContent(news: news[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = news.endIndex - 1
        
        if indexPath.row == lastElement {
            eventHandler?.loadNextPage()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}


// MARK: NewsFeedViewInterface

extension NewsFeedViewController: NewsFeedViewInterface {

    func showLoading(_ show: Bool) {
        backgroundLayerView.isHidden = !show
        show ? loadingIndicator.startAnimating() : loadingIndicator.stopAnimating()
        loadingIndicator.isHidden = !show
    }
    
    func showNewsFeedContent(news: [News]) {
        newsFeedTableView.isHidden = false
        self.news.append(contentsOf: news)
        newsFeedTableView.reloadData()
    }
    
    func showTryAgainView(_ show: Bool) {
        backgroundLayerView.isHidden = !show
        tryAgainView.isHidden = !show
        newsFeedTableView.isHidden = show
    }
}
