//
//  NewsWebViewController.swift
//  BreakingNewsJS
//
//  Created by Jade Silveira on 24/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import Foundation
import WebKit
import UIKit

class NewsWebViewController: UIViewController, WKUIDelegate, UIWebViewDelegate {
    
    var wkWebView               : WKWebView!
    
    var urlString               : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createWebview()
    }
    
    func createWebview() {
        wkWebView = WKWebView(frame: UIScreen.main.bounds)
        wkWebView.uiDelegate = self
        view.addSubview(wkWebView)
        
        loadUrlInWebView()
    }
    
    func loadUrlInWebView() {
        if let urlString = urlString, let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            wkWebView.load(request)
        } else {
            self.navigationController?.popViewController(animated: true)
            showAlert()
        }
        
        if let urlString = urlString, let url = URL(string: urlString) {
            wkWebView.load(URLRequest(url: url))
            return
        }
        self.navigationController?.popViewController(animated: true)
        showAlert()
    }
    
    func showAlert() {
        let alert = UIAlertController.init(title: "Parece que algo deu errado...", message: "Esta notícia não pode ser lida no momento. Tente novamente mais tarde.", preferredStyle: .alert)
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: {})
        }
    }
}
