//
//  BreakingNewsJSTests.swift
//  BreakingNewsJSTests
//
//  Created by Jade Silveira on 19/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import UIKit

import XCTest
@testable import BreakingNewsJS

class BreakingNewsJSTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNewsFeedResponse() {
        let expectation = self.expectation(description: "response")
        
        NewsFeedService.shared.getNewsFeedByPage(page: 1, success: { (news) in
            XCTAssertFalse(news.isEmpty)
            expectation.fulfill()
            
        }, failure: { (message) in
            XCTAssertFalse(message.isEmpty)
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
    }

    func testConnection() {
        let expectation = self.expectation(description: "loaded")
        
        guard let url = URL(string: "https://g1.globo.com/") else {
            fatalError("URL can't be empty")
        }
        let request = URLRequest(url: url)
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            XCTAssertTrue(error == nil)
            
            if let httpResponse = response as? HTTPURLResponse {
                XCTAssertTrue(httpResponse.statusCode == 200)
            }
            
            expectation.fulfill()
        }
        task.resume()
        
        waitForExpectations(timeout: 5, handler: nil)
    }
}
