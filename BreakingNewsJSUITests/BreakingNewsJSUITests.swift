//
//  BreakingNewsJSUITests.swift
//  BreakingNewsJSUITests
//
//  Created by Jade Silveira on 19/03/19.
//  Copyright © 2019 Jade Silveira. All rights reserved.
//

import XCTest

class BreakingNewsJSUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        app = XCUIApplication()
        continueAfterFailure = false

        app.launchArguments.append("--uitesting")
    }

    func testExpectedFlow() {
        app.launch()
        
        randomSwipeUps(max: 5)
        
        tapFirstDisplayedCell()
        
        waitWebViewLoad(stringCondition: "G1")
        
        randomSwipeUps(max: 3)
        
        tapLeftNavButton()
    }
    
    func tapFirstDisplayedCell() {
        app.tables.firstMatch.cells.firstMatch.tap()
    }
    
    func tapLeftNavButton() {
        let leftNavBarButton = app.navigationBars.children(matching: .button).firstMatch
        XCTAssert(leftNavBarButton.exists)
        leftNavBarButton.tap()
    }
    
    func waitWebViewLoad(stringCondition: String) {
        let condition = app.staticTexts[stringCondition]
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: condition, handler: nil)
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func randomSwipeUps(max: Int) {
        let number = Int.random(in: Range(1...max))
        var index = 0
        
        while index < number {
            app.swipeUp()
            
            index += 1
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
